# Mod dispatcher

This repository contains small script and template file for creation of
singular entries in Dolphin context menu that help with file distribution.

---

## Usage:

Execute script `add-mod-service` with the name of the game and path to
desired directory. As an additional option one can specify one or more mime
types, in order to restrict entry from showing when not needed; add
select icon file either from system icons or as a path to the icon file.

Each entry passed created with flag `--combined` will be combined in one
submenu called "Send mod to". All entries created without `--combined` flag
will be still visible in the main context menu, along with "Send mod to"
submennu when mime types will be compatible.


### Examples:

![Simple entry in a file](.images/single-entry.jpg)

Singular entry visible in context menu.

![Two entries, icon and mime type](.images/two-entries.jpg)

Two entries, one with icon and binded to `application/zip` mime type.

![Entries combined in submenu](.images/combined-entries.jpg)

Two entries combined in a submenu, below visible standalone entry.

## License

This program is under GPLv3 license.
